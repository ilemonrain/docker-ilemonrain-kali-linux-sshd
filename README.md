# Docker Kali Linux with OpenSSH-Server

## The quieter you become, The more you are able to hear.

## 1.镜像标签 (Tag)
> **ilemonrain/kali-linux-sshd**, **ilemonrain/kali-linux-sshd:baseimage** : [Dockerfile](https://bitbucket.org/ilemonrain/docker-ilemonrain-kali-linux-sshd/src/1fb445858237/kali-linux-baseimage/Dockerfile?at=master&fileviewer=file-view-default)  
> **ilemonrain/kali-linux-sshd:metasploit-framework** : [Dockerfile](https://bitbucket.org/ilemonrain/docker-ilemonrain-kali-linux-sshd/src/1fb445858237dd85970cc2c4bcd77502d7231e92/kali-linux-metasploit-framework/Dockerfile?at=master&fileviewer=file-view-default)

## 2. 镜像说明
> 牛逼到不用我解释的，大名鼎鼎的 **[Kali Linux](https://www.kali.org/)** ，相关介绍自己百度去，这里我不多说无关的废话。
>
> 由于制作完整的Kali Linux镜像吨位过于庞大，故只制作了**ilemonrain/kali-linux-sshd:baseimage** 和 **ilemonrain/kali-linux-sshd:metasploit-framework** 这两个镜像。
>
> **BaseImage** 包含了Docker Kali Linux的基础环境，并已配置好repo，请自行使用 **apt-get update && apt-get install [Kali软件包组]** 命令来补全Kali所需的渗透测试工具。
>
> **Metasploit-Framework** 则在 **BaseImage** 的基础上，额外安装好了Metasploit框架，一定程度上做到开包（部署容器）即用，减少安装软件的时间。（但同上，完整的Kali Linux组件仍然需要手动安装。
>
> 关于如何安装Kali Linux的组件，请移步 [Kali Linux 官网相关文章](https://www.kali.org/news/kali-linux-metapackages/) 。

## 3. 使用说明
> 使用如下命令行启动 Docker Kali Linux：
> ```
> docker run -d -p 2222:22 ilemonrain/kali-linux-sshd:baseimage [附加命令行]
> ```
>
> **-d**：后台运行，必须有这个参数，不然会卡死（因为默认的CMD值是“**/usr/sbin/sshd -D**”），如果需要，请在后面的附加命令行区域，输入你想要执行的命令即可，比如 **ps -A** （不推荐这样做，很多事情你可以SSH连接上后疯狂的搞事情，为什么只启动1s就停下来呢？）  
> **-p [外部端口]:[内部端口]**： 端口映射，如果母鸡（Docker宿主机）上的端口被占用，或者想换个端口，加上这个命令行参数即可，比如映射母鸡的2222端口到Docker Kali Linux的22端口（SSH服务），那么请输入“**-p 2222:22**”，想映射多少个端口就加多少个参数。如果需要控制端口的协议类型（TCP/UDP），请在后面加上/TCP，/UDP，例如“**-p 2222:22/tcp**”，“**-p 5353:53/udp**”。    
>**ilemonrain/kali-linux-sshd:baseimage**：你想要启动的镜像，从上面的“1. 镜像标签 (Tag)”中，选择你需要的镜像，进行加载。
>
> **登录用户名：root 登录密码：kali**


**[附加命令行]**：如果你不想启动SSHD（**/usr/bin/sshd -D**），那么你可以在这里替换成你需要的命令，如“ps -A”。

## 4. 免责声明
> 我制作这个镜像，只是出于学习用途，或者网络安全爱好者的研究用途，禁止使用本工具（或者叫此Docker镜像）发动非法攻击！因发动非法攻击，造成损失，甚至被请去喝茶，本人不负任何责任！

## 5. 调戏作者
> 发送E-mail到：ilemonrain@ilemonrain.com